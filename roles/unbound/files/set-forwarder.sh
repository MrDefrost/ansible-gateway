#!/bin/bash

NAMESERVER=$(grep ^nameserver /etc/resolv.conf -m1| awk '{ print $2}')

sed "s/RESOLVER/$NAMESERVER/g" /etc/unbound/unbound.conf.tmpl > /etc/unbound/unbound.conf

# use own dnsserver for resolving
sed -i "s/$NAMESERVER/127.0.0.1/g" /etc/resolv.conf
