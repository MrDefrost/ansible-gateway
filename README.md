dnf install git bash-completion ansible-core setools policycoreutils-python-utils gcc
ansible-galaxy collection install ansible.posix
python3 -m pip install --user ansible-lint
